/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/statvfs.h>
#include <simpleprint/simpleprint.h>

///////////////////////////////////////////////////////////////////////////////
// Color strings
///////////////////////////////////////////////////////////////////////////////
const char setcolor[] = {0x1B, 0x5B, '3', '1', ';', '1', 'm', 0};
const char resetcolor[] = {0x1B, 0x5B, '0', 'm', 0};

///////////////////////////////////////////////////////////////////////////////
// Output printing
///////////////////////////////////////////////////////////////////////////////
void PrintWithLength (char *str, int len)
{
	int n = len;
	int pos = 0;

	do
	{
		int diff = write (1, str + pos, n);

		if (diff < 0)
			return;

		pos += diff;
		n -= diff;
	} while (pos != len);
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void CheckLine (char *buffer, int bufferlength, char *pattern, int patternlength, int istty)
{
	// Number of characters to try
	int n = bufferlength - patternlength + 1;

	// Loop through buffer
	for (int i = 0; i < n; i++)
	{
		// Variable
		int same = 1;

		// Try proving if they're unequal
		for (int j = 0; j < patternlength; j++)
		{
			if (buffer[i+j] != pattern[j])
			{
				same = 0;
				break;
			}
		}

		// If they are equal, print the line with highlight
		if (same)
		{
			if (istty)
			{
				PrintWithLength (buffer, i);
				SimplePrint ((char*)setcolor);
				PrintWithLength (buffer + i, patternlength);
				SimplePrint ((char*)resetcolor);
				PrintWithLength (buffer + i + patternlength, bufferlength - i - patternlength);
				SimplePrint ("\n");
			}
			else
			{
				PrintWithLength (buffer, bufferlength);
				SimplePrint ("\n");
			}

			return;
		}
	}
}

int RunGrep (char *pattern)
{
	// Line buffer, position, remaining bytes, length of pattern and check index
	char linebuffer[512];
	int pos = 0;
	int remaining = 512;
	int patternlength = strlen (pattern);
	int i = 0;

	// Is the input and output a terminal?
	int ti = isatty (0) > 0 ? 1 : 0;
	int to = isatty (1) > 0 ? 1 : 0;

	// Loop forever
	while (1)
	{
		// Check for no more space
		if (!remaining)
		{
			SimplePrint ("grep: dropped line\n");
			pos = 0;
			remaining = 512;
		}

		// Append some bytes
		int n = read (0, linebuffer + pos, remaining);

		// Check for errors or completion
		if (n < 0)
		{
			SimplePrint ("grep: read error\n");
			return 1;
		}
		else if (n == 0 && !ti && i == pos)
			return 0;

		// Update position and remaining
		pos += n;
		remaining -= n;

		// Check for newlines
		for (; i < pos; i++)
		{
			if (linebuffer[i] == '\n' || linebuffer[i] == '\r')
			{
				// Newline found, check line
				linebuffer[i] = 0;

				if (i)
					CheckLine (linebuffer, i, pattern, patternlength, to);

				// Bytes taken
				int taken = i + 1;

				// Move remaining data and set indexes
				memmove (linebuffer, linebuffer + taken, pos - taken);

				pos -= taken;
				remaining += taken;
				i = 0;
				break;
			}
		}
	}
}

int PrintUsage ()
{
	SimplePrint (
		"Usage: grep [args]\n"
		"  --help\n"
		"    Print this message\n");
	return 0;
}

int PrintUnknown (void)
{
	SimplePrint ("Invalid argument, use\n   grep --help\nfor a usage list\n");
	return 1;
}

int main (int argc, char **argv)
{
	if (argc == 2)
	{
		if (!strcmp(argv[1], "--help"))
			return PrintUsage ();
		else
			return RunGrep (argv[1]);
	}

	return PrintUnknown ();
}
